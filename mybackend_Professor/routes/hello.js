const express = require('express')
const router = express.Router()

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.json({ message: 'Hello Hattaya' })
})
router.get('/:message', function (req, res, next) {
  const { params } = req
  res.json({ message: 'Hello Hattaya', params })
})

module.exports = router
