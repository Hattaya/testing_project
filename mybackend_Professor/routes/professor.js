const express = require('express')
const router = express.Router()
const usersController = require('../controller/ProfessorsController')

router.get('/', usersController.getProfessors)

router.get('/:id', usersController.getProfessor)

router.post('/', usersController.addProfessor)

router.put('/', usersController.updateProfessor)

router.delete('/:id', usersController.deleteProfessor)

module.exports = router
