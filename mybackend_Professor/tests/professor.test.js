const dbHandler = require('./db-handler')
const Professor = require('../models/Professor')

beforeAll(async () => {
  await dbHandler.connect()
})
afterEach(async () => {
  await dbHandler.clearDatabase()
})

afterAll(async () => {
  await dbHandler.closeDatabase()
})

const proComplete1 = {
  name: 'Hattaya',
  branch: 'CS',
  phoneNumber: '0861470058',
  email: '60160148@go.buu.ac.th'
}
const proComplete2 = {
  name: 'Hattaya',
  branch: 'IT',
  phoneNumber: '021470058',
  email: '60160148@go.buu.ac.th'
}
const proComplete3 = {
  name: 'Hattaya',
  branch: 'SE',
  phoneNumber: '0861470058',
  email: '60160148@go.buu.ac.th'
}

const proErrorName = {
  name: '',
  branch: 'CS',
  phoneNumber: '0861470058',
  email: '60160148@go.buu.ac.th'
}

const proErrorBranch = {
  name: 'Hattaya',
  branch: '',
  phoneNumber: '0861470058',
  email: '60160148@go.buu.ac.th'
}

const proErrorPhone = {
  name: 'Hattaya',
  branch: 'CS',
  phoneNumber: '',
  email: '60160148@go.buu.ac.th'
}

const proErrorEmail = {
  name: 'Hattaya',
  branch: 'SE',
  phoneNumber: '0861470058',
  email: ''
}

const proErrorNameandBranch = {
  name: '',
  branch: '',
  phoneNumber: '0861470058',
  email: '60160148@go.buu.ac.th'
}
const proErrorNameandPhone = {
  name: '',
  branch: 'CS',
  phoneNumber: '',
  email: '60160148@go.buu.ac.th'
}
const proErrorNameandEmail = {
  name: 'Hattaya',
  branch: 'SE',
  phoneNumber: '0861470058',
  email: ''
}

const proErrorBranchandPhone = {
  name: 'Hattaya',
  branch: '',
  phoneNumber: '',
  email: '60160148@go.buu.ac.th'
}

const proErrorBranchandEmail = {
  name: 'Hattaya',
  branch: '',
  phoneNumber: '0861470058',
  email: ''
}

const proErrorPhoneandEmail = {
  name: 'Hattaya',
  branch: 'CS',
  phoneNumber: '0861470058',
  email: ''
}
const proEmail = {
  name: '',
  branch: '',
  phoneNumber: '',
  email: '60160148@go.buu.ac.th'
}
const proPhone = {
  name: '',
  branch: '',
  phoneNumber: '0861470058',
  email: ''
}
const proBranch= {
  name: '',
  branch: 'CS',
  phoneNumber: '',
  email: ''
}

const proName = {
  name: 'Hattaya',
  branch: 'CS',
  phoneNumber: '0861470058',
  email: '60160148@go.buu.ac.th'
}

const proError = {
  name: '',
  branch: '',
  phoneNumber: '',
  email: ''
}
const proErrorName5charater = {
  name: 'hat',
  branch: 'CS',
  phoneNumber: '0861470058',
  email: '60160148@go.buu.ac.th'
}

const proErrorBranchinvalid = {
  name: 'Hattaya',
  branch: 'S',
  phoneNumber: '0861470058',
  email: '60160148@go.buu.ac.th'
}
const proErrorPhonenumberinvalid = {
  name: 'Hattaya',
  branch: 'S',
  phoneNumber: '08614700',
  email: '60160148@go.buu.ac.th'
}



describe('Professor', () => {
  it('สามารถเพิ่ม professor ได้ CS ', async () => {
    let error = null
    try {
      const user = new Professor(proComplete1)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).toBeNull()
  })
  it('สามารถเพิ่ม professor ได้ IT ', async () => {
    let error = null
    try {
      const user = new Professor(proComplete2)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).toBeNull()
  })
  it('สามารถเพิ่ม professor ได้  SE', async () => {
    let error = null
    try {
      const user = new Professor(proComplete3)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ name เป็นช่องว่าง', async () => {
    let error = null
    try {
      const user = new Professor(proErrorName)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ branch เป็นช่องว่าง', async () => {
    let error = null
    try {
      const user = new Professor(proErrorBranch)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ phonenumber เป็นช่องว่าง', async () => {
    let error = null
    try {
      const user = new Professor(proErrorPhone)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ email เป็นช่องว่าง', async () => {
    let error = null
    try {
      const user = new Professor(proErrorEmail)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ name and branch เป็นช่องว่าง', async () => {
    let error = null
    try {
      const user = new Professor(proErrorNameandBranch)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ name and phonenumber เป็นช่องว่าง', async () => {
    let error = null
    try {
      const user = new Professor(proErrorNameandPhone)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ name and email เป็นช่องว่าง', async () => {
    let error = null
    try {
      const user = new Professor(proErrorNameandEmail)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ beanch and phonenumber  เป็นช่องว่าง', async () => {
    let error = null
    try {
      const user = new Professor(proErrorBranchandPhone)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ branch and email เป็นช่องว่าง', async () => {
    let error = null
    try {
      const user = new Professor(proErrorBranchandEmail)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ phonenumber and email เป็นช่องว่าง', async () => {
    let error = null
    try {
      const user = new Professor(proErrorPhoneandEmail)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ  กรอกแต่ email ', async () => {
    let error = null
    try {
      const user = new Professor(proEmail)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ  กรอกแต่ phonenumber ', async () => {
    let error = null
    try {
      const user = new Professor(proPhone)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ  กรอกแต่ branch ', async () => {
    let error = null
    try {
      const user = new Professor(proBranch)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ  กรอกแต่ name ', async () => {
    let error = null
    try {
      const user = new Professor(proName)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ  ไม่กรอก', async () => {
    let error = null
    try {
      const user = new Professor(proError)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ name เป็น 4 charater', async () => {
    let error = null
    try {
      const user = new Professor(proErrorName5charater)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ branch ไม่ถูกต้อง', async () => {
    let error = null
    try {
      const user = new Professor(proErrorBranchinvalid)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่ม professor  ได้ เพราะ Phonenumber ไม่ถูกต้อง', async () => {
    let error = null
    try {
      const user = new Professor(proErrorPhonenumberinvalid)
      await user.save()
    } catch (err) {
      error = err
    }
    expect(error).not.toBeNull()
  })

})
