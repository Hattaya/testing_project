const mongoose = require('mongoose')
const professorSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 6
  },
  branch: {
    type: String,
    required: true,
    enum: ['CS', 'IT', 'SE']
  },
  phoneNumber: {
    type: String,
    required: true,
    minlength: 9,
    maxlength: 10
  },
  email: {
    type: String,
    required: true
  }
})

module.exports = mongoose.model('Professor', professorSchema)
